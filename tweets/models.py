from django.conf import settings
from django.db import models


class Tweet(models.Model):
    """
    Stores a single tweet entry, related to `auth.User`.
    """
    content = models.CharField(max_length=settings.TWEET_MAX_LENGTH)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)

    def __str__(self):
        return self.content

    class Meta:
        ordering = ['-created']
