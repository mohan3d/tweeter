Tweeter
=======

A restful api written in `python`_ and `django`_ to serve `Tweeter-Interface`_.


Install Dependencies
--------------------

.. code-block:: bash

    $ pip install -r requirements.txt


Deployment
----------

.. code:: bash

    $ python manage.py migrate

**Note**: user must be created in order to test the task, the easiest way to do so is to
create super user.

.. code:: bash

    $ python manage.py createsuperuser


Development server
------------------

.. code:: bash

    $ python manage.py runserver


Project structure
-----------------
::

    .
    ├── api                             # api application contains everything related to api
    │   └── rest                        # all restful APIs versions will live here
    │       └── v1                      # api version 1
    │           ├── mixins.py           # mixins (reusable behaviours of views)
    │           ├── serializers.py      # contains classes that serialize objects to json/xml and vice versa
    │           ├── urls.py             # api version 1 exposed urls
    │           └── views.py            # views to process requests
    │
    ├── tweets                          # tweets application contains Tweet model and migrations.
    │   ├── migrations                  # models migrations related to all models in tweets app
    │   ├── admin.py                    # admin view for tweets application contains registered models
    │   ├── apps.py                     # entry point for django project (used in tweeter.settings)
    │   ├── models.py                   # contains all models related to tweet app (Tweet model)
    │   ├── tests.py                    # tweets app tests
    │   └── views.py                    # tweets app views
    │
    ├── tweeter                         # root application.
    │   ├── settings.py                 # contains settings and configurations for all applications
    │   ├── urls.py                     # public and accessible urls
    │   └── wsgi.py                     # entry point for wsgi
    │
    ├── requirements.txt                # project dependencies
    ├── manage.py                       # entry point for django cli commands
    └── README.rst

.. _python: https://www.python.org/
.. _django: https://www.djangoproject.com/
.. _Tweeter-Interface: https://bitbucket.org/mohan3d/tweeter-interface
