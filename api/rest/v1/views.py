from rest_framework import generics

from .mixins import AuthenticatedUserTweetsMixin
from .serializers import TweetSerializer


class TweetList(AuthenticatedUserTweetsMixin,
                generics.ListCreateAPIView):
    """
    Serves GET/POST requests for getting list of tweets and creating new tweet.
    """
    serializer_class = TweetSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TweetDetail(AuthenticatedUserTweetsMixin,
                  generics.RetrieveDestroyAPIView):
    """
    Serves GET/DELETE requests for getting and deleting a tweet.
    """
    serializer_class = TweetSerializer
