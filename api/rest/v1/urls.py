from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from .views import TweetList, TweetDetail

urlpatterns = [
    path('login/', obtain_auth_token, name='login'),
    path('tweets/', TweetList.as_view(), name='tweet-list'),
    path('tweets/<int:pk>/', TweetDetail.as_view(), name='tweet-details'),
]
