from rest_framework import serializers

from tweets.models import Tweet


class TweetSerializer(serializers.ModelSerializer):
    """
    Serializes Tweet object into different formats (json and xml)
    and json/xml into Tweet object.
    """

    class Meta:
        model = Tweet
        fields = ('id', 'content', 'created')
