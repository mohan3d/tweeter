from tweets.models import Tweet


class AuthenticatedUserTweetsMixin:
    """
    A mixin that can be used to query authenticated user's tweet list.
    """

    def get_queryset(self):
        """
        Returns queryset includes only logged-in user's tweets.
        """
        return Tweet.objects.filter(owner=self.request.user)
